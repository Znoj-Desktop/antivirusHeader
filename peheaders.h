#ifndef PEHEADERS_H
#define PEHEADERS_H

#include <QDebug>

//#pragma once
#include "libs/peheaders/stdafx.h"
#include "libs/peheaders/resource.h"
#include "libs/peheaders/ITLib.h"

extern	HINSTANCE hInst;// current instance

extern	HWND	hwndMain;
extern	HMENU	hMenu;

extern	HWND	hButton;

extern ITLibrary 	*itlib;
extern BOOL			bFileOpen;

extern char cFnameOpen[256];

char* IntToHex(DWORD dwValue,UCHAR Num,BOOL HexSign);
HTREEITEM AddItemToTree(HWND hwndTV, LPSTR lpszItem, int nLevel);


#include <QString>
#include <QByteArray>
#include <QList>
#include <QPair>


using namespace std;

class PeHeaders
{
    QString path;

public:
    typedef QList<QPair<QString, QList<QString>> > TLibsFunctionsList;  // TODO: FIX - the same as in "model.h" -> delete here, but cause cycling dependencies

    PeHeaders(const QString path);

    TLibsFunctionsList libsFunctions();

private:
    vector<char *> getLibraries(char *fileName);
    vector<char *> getFunctions(char *fileName, bool includeLibName = false);
};

#endif // PEHEADERS_H
