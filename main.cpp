/****************************************************************************
**
** Copyright (C) 2016 The Qt Company Ltd.
** Contact: https://www.qt.io/licensing/
**
** This file is part of the documentation of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:BSD$
** Commercial License Usage
** Licensees holding valid commercial Qt licenses may use this file in
** accordance with the commercial license agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and The Qt Company. For licensing terms
** and conditions see https://www.qt.io/terms-conditions. For further
** information use the contact form at https://www.qt.io/contact-us.
**
** BSD License Usage
** Alternatively, you may use this file under the terms of the BSD license
** as follows:
**
** "Redistribution and use in source and binary forms, with or without
** modification, are permitted provided that the following conditions are
** met:
**   * Redistributions of source code must retain the above copyright
**     notice, this list of conditions and the following disclaimer.
**   * Redistributions in binary form must reproduce the above copyright
**     notice, this list of conditions and the following disclaimer in
**     the documentation and/or other materials provided with the
**     distribution.
**   * Neither the name of The Qt Company Ltd nor the names of its
**     contributors may be used to endorse or promote products derived
**     from this software without specific prior written permission.
**
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
**
** $QT_END_LICENSE$
**
****************************************************************************/

//! [main program]
#include <QtWidgets>
#include <QDir>
#include <QStringList>
#include <QVector>
#include <QString>
#include "PEHeader.cpp"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    QWidget window;
    window.resize(580, 240);
    window.setWindowTitle
          (QApplication::translate("childwidget", "Child widget"));
    window.show();

//! [create, position and show]
    QPushButton *button = new QPushButton(
        QApplication::translate("childwidget", "Press me"), &window);
    button->show();
    button->move(0, 2);
    button->setText("Get Libraries");


    QTextEdit *text_field = new QTextEdit(
                QApplication::translate("childwidget", ""), &window);
    text_field->move(30, 30);
    text_field->show();


    QPushButton *button2 = new QPushButton(
        QApplication::translate("childwidget", "Press me"), &window);
    button2->move(300, 2);
    button2->show();
    button2->setText("Get Functions");

    QTextEdit *text_field2 = new QTextEdit(
                QApplication::translate("childwidget", ""), &window);
    text_field2->move(300, 30);
    text_field2->show();

    QCheckBox *checkBox = new QCheckBox(
                QApplication::translate("childwidget", ""), &window);
    checkBox->move(380, 5);
    checkBox->setText("With library");
    checkBox->show();

    const char *path = "c:\\testing\\";
    QObject::connect(button, &QPushButton::clicked, [=]() {
        QDir recoredDir(path);
        QStringList allFiles = recoredDir.entryList();

        QString text = "";
        QString fileName = "";
        std::vector<char *> librariesList;
        for(int i = 0; i < allFiles.length(); i++){
            fileName = allFiles.value(i);

               text += "libraries in file \"" + allFiles.value(i) + "\":\n";

               QString ba = path + allFiles.value(i);
               QByteArray array = ba.toLocal8Bit();
               //get all libraries
               librariesList = getLibraries(array.data());

               for(std::vector<char *>::iterator it = librariesList.begin(); it != librariesList.end(); ++it){
                   //printf("%s\n", *it);
                   text += *it;
                   text += "\n";
               }
               text += "\n";
               freeMemory();

        }
        text_field->setText(text);

       });



    QObject::connect(button2, &QPushButton::clicked, [=]() {
        QDir recoredDir(path);
        QStringList allFiles = recoredDir.entryList();

        QString text = "";
        QString fileName = "";
        std::vector<char *> librariesList2;
        for(int i = 0; i < allFiles.length(); i++){
            fileName = allFiles.value(i);
            //if(fileName.contains("exe")){
               text += "libraries in file \"" + allFiles.value(i) + "\":\n";

               QString ba = path + allFiles.value(i);
               QByteArray array = ba.toLocal8Bit();

               if(checkBox->checkState()){
                   //get all functions like "library>function"
                   librariesList2 = getFunctions(array.data(), true);
               }
               else{
                   //get all functions
                   librariesList2 = getFunctions(array.data());
               }


               for(std::vector<char *>::iterator it = librariesList2.begin(); it != librariesList2.end(); ++it){
                   //printf("%s\n", *it);
                   text += *it;
                   text += "\n";
               }
               text += "\n";
               freeMemory();

           // }
        }
        text_field2->setText(text);

       });

//! [create, position and show]
    return app.exec();
}
//! [main program]
