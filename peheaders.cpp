#include "peheaders.h"

//#define  WIN32_LEAN_AND_MEAN
#include <stdio.h>
#include <stdlib.h>
#include <shellapi.h>
#include <windowsx.h>
#include <vector>


#pragma comment(linker,"/BASE:0x400000 /FILEALIGN:0x200 /MERGE:.rdata=.text /MERGE:.data=.text /SECTION:.text,EWR /IGNORE:4078")
#pragma pack(1)

#ifdef AP_UNIX_STYLE
   #include <unistd.h>
   #define CB_CALLCONV
#else
   #include <io.h>
   #define AP_HAS_CONIO
   #ifdef AP_DLL
      #define CB_CALLCONV __stdcall
   #else
      #define CB_CALLCONV __cdecl
   #endif
#endif


PeHeaders::PeHeaders(const QString path) : path(path)
{
}


PeHeaders::TLibsFunctionsList PeHeaders::libsFunctions()
{
    TLibsFunctionsList out;

    ITLibrary *itlib;
    itlib = new (ITLibrary);
    //itlib->OpenFileName(path.toStdString().c_str());
    //std::string utf8Filename = path.toUtf8().constData();
    QByteArray utf8Filename = path.toLocal8Bit();
    long size = itlib->OpenFileName(utf8Filename.data());

    if (size <= 0) {
        delete itlib;
        return out;
    }

    // LIBS
    std::pair<std::vector<char*>, int> libs = itlib->GetImportDllName();
    // IMPORTANT - WHEN BROKEN PE HEADER, SKIP THIS SAMPLE
    if (libs.second < 0) {
        delete itlib;
        return TLibsFunctionsList();
    }

    for (int i = 0; i < libs.first.size(); i++) {
        QString lib = QString::fromStdString(libs.first.at(i)).trimmed();
        /*if (lib.isEmpty()) { // skip if line is empty
            // TODO: MAKE SOME ALLERT
            continue;
        }*/
        out.append(qMakePair(lib, QList<QString>()));

        // FUNCTIONS
        std::pair<std::vector<char*>, int> functions = itlib->GetImportProcName(i);
        // IMPORTANT - WHEN BROKEN PE HEADER, SKIP THIS SAMPLE
        if (libs.second < 0) {
            delete itlib;
            return TLibsFunctionsList();
        }
        
        for(int j = 0; j < functions.first.size(); j++) {
            QString function = QString::fromStdString(functions.first.at(j)).trimmed();
            /*if (function.isEmpty()) { // skip if line is empty
                // TODO: MAKE SOME ALLERT
                continue;
            }*/
            out.last().second.append(function);
        }
    }

    delete itlib;
    return out;
}

vector<char *> PeHeaders::getLibraries(char *fileName) {

    ITLibrary *itlib;
    itlib=new (ITLibrary);
    
    std::pair<std::vector<char*>, int> out;

    //printf("Opening file %s ", fileName);
    long size = itlib->OpenFileName(fileName);
    //printf("with size %d B\n", size);
    if (size == 0) {
        delete itlib;
        return out.first;
    }

    out = itlib->GetImportDllName();
    // IMPORTANT - WHEN BROKEN PE HEADER, SKIP THIS SAMPLE
    if (out.second < 0) {
        delete itlib;
        return std::vector<char*>();
    }

    /*
    for(std::vector<char *>::iterator it = v.first.begin(); it != v.first.end(); ++it){
        printf("%s\n", *it);
    }
    */

    //itlib->~ITLibrary();
    delete itlib;
    return out.first;
}

vector<char *> PeHeaders::getFunctions(char *fileName, bool includeLibName){

    ITLibrary *itlib;
    itlib=new (ITLibrary);
    
    std::vector<char*> out;
    
    long size = itlib->OpenFileName(fileName);
    if (size == 0) {
        delete itlib;
        return out;
    }

    std::pair<std::vector<char*>, int> libs = itlib->GetImportDllName();
    // IMPORTANT - WHEN BROKEN PE HEADER, SKIP THIS SAMPLE
    if (libs.second < 0) {
        delete itlib;
        return out;
    }

    for(long i = 0; i < libs.first.size(); i++){
        std::pair<std::vector<char*>, int> functions = itlib->GetImportProcName(i);
        // IMPORTANT - WHEN BROKEN PE HEADER, SKIP THIS SAMPLE
        if (functions.second < 0) {
            delete itlib;
            return std::vector<char*>();
        }
        
        if(includeLibName) {
            for(int j = 0; j < functions.first.size(); j++) {
                char* strTmp = (char*) malloc( 2 + strlen(libs.first.at(i))+ strlen(functions.first.at(j)) );
                strcpy(strTmp, libs.first.at(i));
                strcat(strTmp, ">");
                strcat(strTmp, functions.first.at(j));
                out.push_back(strTmp);
            }
        } else {
            for(int j = 0; j < functions.first.size(); j++) {
                out.push_back(functions.first.at(j));
            }
        }
    }
    
    /*
    for(std::vector<char *>::iterator it = v.begin(); it != v.end(); ++it){
        printf("%s\n", *it);
    }
    */

    //itlib->~ITLibrary();
    delete itlib;
    return out;
}
