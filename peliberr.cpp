/* peliberr.cpp --

   This file is part of the "PE Maker".

   Copyright (C) 2005-2006 Ashkbiz Danehkar
   All Rights Reserved.

   "PE Maker" library are free software; you can redistribute them
   and/or modify them under the terms of the GNU General Public License as
   published by the Free Software Foundation.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; see the file COPYRIGHT.TXT.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   yodap's Site:
   http://yodap.sourceforge.net

   Ashkbiz Danehkar
   <ashkbiz@yahoo.com>
*/
#include "stdafx.h"
#include "PELibErr.h"

const char	*szFileErr			="File access error :(";
const char	*szNoPEErr			="Invalid PE file!";
const char	*szNoMemErr			="Not enough memory :(";
const char	*szFsizeErr			="Files with a filesize of 0 aren't allowed!";
const char	*szNoRoom4SectionErr="There's no room for a new section :(";
const char	*szSecNumErr		="Too many sections!";
const char	*szIIDErr			="Too much ImageImportDescriptors!";
const char	*szFileISProtect	="File already was protected!";
const char	*szPEnotValid		="Invalid PE file! It might be protected by another tool.";
const char	*szPEisCOMRuntime	="This Version does not support COM Runtime structure.";
const char	*szDLLnotSupport	="This Version does not support dynamic link library.";
const char	*szWDMnotSupport	="This Version does not support windows driver model.";
const char	*szTServernotSupport="This Version does not support terminal server aware.";
const char	*szSYSnotSupport	="This Version does not support system file.";
const char	*szNOSEHnotSupport	="No SE handler resides in this PE.";
const char	*szNOBINDnotSupport	="Can not support PE file with no bind.";
const char	*szPackSectionName	="Section's Name is not recognized :(";

//void ShowErr(unsigned char numErr);

//----------------------------------------------------------------
//----- ERROR MESSAGES ----
//The ShowErr display message by receiving its Error Number
void ShowErr(unsigned char numErr)
{
    qDebug() << GetErr(numErr);
}

const char* GetErr(unsigned char numErr)
{
    //char *szErr=new CHAR[64];
	switch(numErr)
	{
	case MemErr:
        return szNoMemErr;
        //strcpy(szErr,szNoMemErr);
        //break;

	case PEErr:
        return szNoPEErr;
        //strcpy(szErr,szNoPEErr);
        //break;

	case FileErr:
        return szFileErr;
        //strcpy(szErr,szFileErr);
        //break;

	case NoRoom4SectionErr:
        return szNoRoom4SectionErr;
        //strcpy(szErr,szNoRoom4SectionErr);
        //break;

	case FsizeErr:
        return szFsizeErr;
        //strcpy(szErr,szFsizeErr);
        //break;

	case SecNumErr:
        return szSecNumErr;
        //strcpy(szErr,szSecNumErr);
        //break;

	case IIDErr:
        return szIIDErr;
        //strcpy(szErr,szIIDErr);
        //break;

	case FileISProtect:
        return szFileISProtect;
        //strcpy(szErr,szFileISProtect);
        //break;

	case PEnotValid:
        return szPEnotValid;
        //strcpy(szErr,szPEnotValid);
        //break;
		
	case PEisCOMRuntime:
        return szPEisCOMRuntime;
        //strcpy(szErr,szPEisCOMRuntime);
        //break;

	case DLLnotSupport:
        return szDLLnotSupport;
        //strcpy(szErr,szDLLnotSupport);
        //break;

	case WDMnotSupport:
        return szWDMnotSupport;
        //strcpy(szErr,szWDMnotSupport);
        //break;

	case TServernotSupport:
        return szTServernotSupport;
        //strcpy(szErr,szTServernotSupport);
        //break;

	case SYSnotSupport:
        return szSYSnotSupport;
        //strcpy(szErr,szSYSnotSupport);
        //break;

	case NOSEHnotSupport:
        return szNOSEHnotSupport;
        //strcpy(szErr,szNOSEHnotSupport);
        //break;

	case NOBINDnotSupport:
        return szNOBINDnotSupport;
        //strcpy(szErr,szNOBINDnotSupport);
        //break;

	case PackSectionName:
        return szPackSectionName;
        //strcpy(szErr,szPackSectionName);
        //break;


    default:
        return nullptr;
	}
    //MessageBoxA(GetActiveWindow(),szErr,
    //		   "Error",
    //		   MB_OK | MB_ICONERROR );
}
