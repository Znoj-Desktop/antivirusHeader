/* ITlib.h --

   This file is part of the "PE Maker".

   Copyright (C) 2005-2006 Ashkbiz Danehkar
   All Rights Reserved.

   "PE Maker" library are free software; you can redistribute them
   and/or modify them under the terms of the GNU General Public License as
   published by the Free Software Foundation.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; see the file COPYRIGHT.TXT.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   Ashkbiz Danehkar
   <ashkbiz@yahoo.com>
*/
#pragma once
#define MAX_SECTION_NUM         20
#include <vector>
//----------------------------------------------------------------
class ITLibrary
{
protected:
private:
public:
	DWORD					dwFileSize;
	char					*pMem;
	ITLibrary();
	~ITLibrary();
	DWORD PEAlign(DWORD dwTarNum,DWORD dwAlignTo);
    PIMAGE_SECTION_HEADER ImageRVA2Section(PIMAGE_NT_HEADERS32 pimage_nt_headers, DWORD dwRVA);
	PIMAGE_SECTION_HEADER ImageRVA2Section64(PIMAGE_NT_HEADERS64 pimage_nt_headers, DWORD dwRVA);
	DWORD RVA2Offset(PCHAR pImageBase,DWORD dwRVA);
	DWORD RVA2Offset64(PCHAR pImageBase, DWORD dwRVA);
	long OpenFileName(char* FileName);

    //bool isStructureOkay(PIMAGE_IMPORT_DESCRIPTOR pimage_import_descriptor);
    //bool isNameOkay(PCHAR pImageBase, DWORD rVA2Offset);

	std::pair<std::vector<char*>, int> GetImportDllName();
	std::pair<std::vector<char*>, int> GetImportProcName(int iModule);
};
//----------------------------------------------------------------
