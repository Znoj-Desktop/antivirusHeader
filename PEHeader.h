#pragma once
#include "stdafx.h"
#include "resource.h"
#include "ITLib.h"

extern	HINSTANCE hInst;// current instance

extern	HWND	hwndMain; 
extern	HMENU	hMenu;

extern	HWND	hButton;

extern ITLibrary 	*itlib;
extern BOOL			bFileOpen;

extern char cFnameOpen[256];

char* IntToHex(DWORD dwValue,UCHAR Num,BOOL HexSign);
HTREEITEM AddItemToTree(HWND hwndTV, LPSTR lpszItem, int nLevel);
