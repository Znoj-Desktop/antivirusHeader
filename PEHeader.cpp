//#define  WIN32_LEAN_AND_MEAN
#include "stdafx.h"
#include "PEheader.h"
#include <stdio.h>
#include <stdlib.h>
#include <shellapi.h>
#include <windowsx.h>
#include <vector>


#pragma comment(linker,"/BASE:0x400000 /FILEALIGN:0x200 /MERGE:.rdata=.text /MERGE:.data=.text /SECTION:.text,EWR /IGNORE:4078")
#pragma pack(1)

#ifdef AP_UNIX_STYLE
   #include <unistd.h>
   #define CB_CALLCONV
#else
   #include <io.h>
   #define AP_HAS_CONIO
   #ifdef AP_DLL
      #define CB_CALLCONV __stdcall
   #else
      #define CB_CALLCONV __cdecl
   #endif
#endif


using namespace std;
ITLibrary *itlib;

static void freeMemory(){
    itlib->~ITLibrary();
}


static vector<char *> getLibraries(char *fileName){

    itlib=new (ITLibrary);
    //printf("Opening file %s ", fileName);
    long size = itlib->OpenFileName(fileName);
    //printf("with size %d B\n", size);
	vector<char *> v;

	if (size == 0) {
		return v;
	}
	v = itlib->GetImportDllName();
	
	/*
    for(std::vector<char *>::iterator it = v.begin(); it != v.end(); ++it){
        printf("%s\n", *it);
    }
	*/

    //itlib->~ITLibrary();
    return v;
}

static vector<char *> getFunctions(char *fileName, bool includeLibName = false){

    itlib=new (ITLibrary);
	long size = itlib->OpenFileName(fileName);
	vector<char *> v;
	if (size == 0) {
		return v;
	}
	v = itlib->GetImportDllName();

    size = v.size();

    vector<char *> v2;
    vector<char *> vtemp;
    for(long i = 0; i < size; i++){
        vtemp = itlib->GetImportProcName(i);
        if(includeLibName){
            for(int k = 0; k < vtemp.size(); k++){
                char * str3 = (char *) malloc(2 + strlen(v.at(i))+ strlen(vtemp.at(k)) );
                strcpy(str3, v.at(i));
                strcat(str3, ">");
                strcat(str3, vtemp.at(k));
                vtemp.at(k) = str3;
            }
        }
        v2.insert(v2.end(), vtemp.begin(), vtemp.end());
        //v.insert(v.end(), v2.begin(), v2.end());
    }
	/*
    for(std::vector<char *>::iterator it = v.begin(); it != v.end(); ++it){
        printf("%s\n", *it);
    }
	*/

    //itlib->~ITLibrary();
    return v2;
}
