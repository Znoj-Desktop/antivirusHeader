/* ITlib.cpp --

   This file is part of the "PE Maker".

   Copyright (C) 2005-2006 Ashkbiz Danehkar
   All Rights Reserved.

   "PE Maker" library are free software; you can redistribute them
   and/or modify them under the terms of the GNU General Public License as
   published by the Free Software Foundation.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; see the file COPYRIGHT.TXT.
   If not, write to the Free Software Foundation, Inc.,
   59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.

   Ashkbiz Danehkar
   <ashkbiz@yahoo.com>
   Modified By Ing. Jiri Znoj jiri@znoj.cz
*/
#include "stdafx.h"
#include <winnt.h>
#include <imagehlp.h>//#include <Dbghelp.h>
//#include <string.h>
#include <Winreg.h>
//#include <commctrl.h>
#include "ITLib.h"
#include "PELibErr.h"
#include <vector>

//----------------------------------------------------------------
//------- DATA ---------
//HANDLE	pMap            = NULL;
DWORD	dwBytesRead		= 0;
DWORD	dwBytesWritten          = 0;
DWORD	dwFsize			= 0;
HANDLE	hFile			= NULL;
//----------------------------
//------- FUNCTION ---------
//----------------------------------------------------------------
ITLibrary::ITLibrary()
{
    pMem=NULL;
}
//----------------------------------------------------------------
ITLibrary::~ITLibrary()
{
    if(pMem!=NULL) GlobalFree(pMem);
}
//----------------------------------------------------------------
long ITLibrary::OpenFileName(char* FileName)
{
    pMem=NULL;
    wchar_t* FileNameW = new wchar_t[4096];
    MultiByteToWideChar(CP_ACP, 0, FileName, -1, FileNameW, 4096);

    hFile=CreateFile(FileNameW,
                     GENERIC_READ,
                     FILE_SHARE_WRITE | FILE_SHARE_READ,
                     NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL,NULL);
    if(hFile==INVALID_HANDLE_VALUE)
    {
        ShowErr(FileErr);
        return -1;
    }
    dwFsize=GetFileSize(hFile,0);
    if(dwFsize == 0)
    {
        CloseHandle(hFile);
        ShowErr(FsizeErr);
        return 0;
    }
    pMem=(char*)GlobalAlloc(GMEM_FIXED | GMEM_ZEROINIT,dwFsize);
    if(pMem == NULL)
    {
        CloseHandle(hFile);
        ShowErr(MemErr);
        return -1;
    }
    ReadFile(hFile,pMem,dwFsize,&dwBytesRead,NULL);
    CloseHandle(hFile);
    dwFileSize=dwFsize;
    return dwFileSize;
}
//----------------------------------------------------------------
/*
bool ITLibrary::isStructureOkay(PIMAGE_IMPORT_DESCRIPTOR pimage_import_descriptor){
    __try
    {
        if(IsBadReadPtr(pimage_import_descriptor, sizeof(PIMAGE_IMPORT_DESCRIPTOR))){
            return false;
        }
        if(pimage_import_descriptor->Name == 0){
            return false;
        }
    }
    __except(EXCEPTION_EXECUTE_HANDLER)
    {
        if(EXCEPTION_ACCESS_VIOLATION == GetExceptionCode())
        {
            printf("Exception: EXCEPTION_ACCESS_VIOLATION\n");
        }
        return false;

    }
    return true;
}
*/
/*
bool ITLibrary::isNameOkay(PCHAR pImageBase, DWORD rVA2Offset){
    __try
    {
        PCHAR pAPIName = pImageBase + rVA2Offset + 2;
        if(IsBadReadPtr(pAPIName, sizeof(CHAR))){
            return false;
        }
        if(pAPIName[0] == NULL){
            return false;
        }
    }
    __except(EXCEPTION_EXECUTE_HANDLER)
    {
        if(EXCEPTION_ACCESS_VIOLATION == GetExceptionCode())
        {
            printf("Exception: EXCEPTION_ACCESS_VIOLATION\n");
        }
        return false;

    }
    return true;
}
*/

// returns aligned value
DWORD ITLibrary::PEAlign(DWORD dwTarNum,DWORD dwAlignTo)
{
    DWORD dwtemp;
    dwtemp=dwTarNum/dwAlignTo;
    if((dwTarNum%dwAlignTo)!=0)
    {
        dwtemp++;
    }
    dwtemp=dwtemp*dwAlignTo;
    return(dwtemp);
}
//================================================================
//----------------------------------------------------------------
PIMAGE_SECTION_HEADER ITLibrary::ImageRVA2Section(PIMAGE_NT_HEADERS32 pimage_nt_headers,DWORD dwRVA)
{
    int i;
    PIMAGE_SECTION_HEADER pimage_section_header=(PIMAGE_SECTION_HEADER)((PCHAR(pimage_nt_headers)) + sizeof(IMAGE_NT_HEADERS32));
    for(i=0;i<pimage_nt_headers->FileHeader.NumberOfSections;i++)
    {
        if((pimage_section_header->VirtualAddress) && (dwRVA<=(pimage_section_header->VirtualAddress+pimage_section_header->SizeOfRawData)))
        {
            return ((PIMAGE_SECTION_HEADER)pimage_section_header);
        }
        pimage_section_header++;
    }
    return(NULL);
}

PIMAGE_SECTION_HEADER ITLibrary::ImageRVA2Section64(PIMAGE_NT_HEADERS64 pimage_nt_headers64, DWORD dwRVA)
{
    int i;
    PIMAGE_SECTION_HEADER pimage_section_header = (PIMAGE_SECTION_HEADER)((PCHAR(pimage_nt_headers64)) + sizeof(IMAGE_NT_HEADERS64));
    for (i = 0; i<pimage_nt_headers64->FileHeader.NumberOfSections; i++)
    {
        if ((pimage_section_header->VirtualAddress) && (dwRVA <= (pimage_section_header->VirtualAddress + pimage_section_header->SizeOfRawData)))
        {
            return ((PIMAGE_SECTION_HEADER)pimage_section_header);
        }
        pimage_section_header++;
    }
    return(NULL);
}
//================================================================
//----------------------------------------------------------------
// calulates the Offset from a RVA
// Base    - base of the MMF
// dwRVA - the RVA to calculate
// returns 0 if an error occurred else the calculated Offset will be returned
DWORD ITLibrary::RVA2Offset(PCHAR pImageBase,DWORD dwRVA)
{
    DWORD _offset;
    PIMAGE_SECTION_HEADER section;
    PIMAGE_DOS_HEADER pimage_dos_header;
    PIMAGE_NT_HEADERS32 pimage_nt_headers;
    pimage_dos_header = PIMAGE_DOS_HEADER(pImageBase);
    pimage_nt_headers = (PIMAGE_NT_HEADERS32)(pImageBase+pimage_dos_header->e_lfanew);
    section=ImageRVA2Section(pimage_nt_headers,dwRVA);
    if(section==NULL || section->SizeOfRawData == 0)
    {
        return(0);
    }
    else if((section->PointerToRawData + section->SizeOfRawData) > dwFileSize){
        //broken PE HEADER DETECTED
        return(-1);
    }
    _offset=dwRVA+section->PointerToRawData-section->VirtualAddress;
    if(_offset > dwFileSize){
        //broken PE HEADER DETECTED
        return(-1);
    }
    return(_offset);
}

DWORD ITLibrary::RVA2Offset64(PCHAR pImageBase, DWORD dwRVA)
{
    DWORD _offset;
    PIMAGE_SECTION_HEADER section;
    PIMAGE_DOS_HEADER pimage_dos_header;
    PIMAGE_NT_HEADERS64 pimage_nt_headers;
    pimage_dos_header = PIMAGE_DOS_HEADER(pImageBase);
    pimage_nt_headers = (PIMAGE_NT_HEADERS64)(pImageBase + pimage_dos_header->e_lfanew);
    section = ImageRVA2Section64(pimage_nt_headers, dwRVA);
    if (section == NULL || section->SizeOfRawData == 0)
    {
        return(0);
    }
    else if((section->PointerToRawData + section->SizeOfRawData) > dwFileSize){
        //broken PE HEADER DETECTED
        return(-1);
    }
    _offset = dwRVA + section->PointerToRawData - section->VirtualAddress;
    if(_offset > dwFileSize){
        //broken PE HEADER DETECTED
        return(-1);
    }
    return(_offset);
}
//----------------------------------------------------------------
std::pair<std::vector<char*>, int> ITLibrary::GetImportDllName()
{
    std::vector<char*> names;

    PCHAR		pThunk;
    DWORD		dwThunk;
    PCHAR		pHintName;
    PCHAR		pDllName;
    DWORD		dwImportDirectory;
    bool		is64 = false;

    PCHAR pImageBase = pMem;
    //----------------------------------------
    PIMAGE_IMPORT_DESCRIPTOR	pimage_import_descriptor;
    PIMAGE_THUNK_DATA           pimage_thunk_data;
    //----------------------------------------
    PIMAGE_DOS_HEADER pimage_dos_header;
    PIMAGE_NT_HEADERS32 pimage_nt_headers;
    //pimage_nt_headers->Signature = NULL;
    PIMAGE_NT_HEADERS64 pimage_nt_headers64;

    pimage_dos_header = PIMAGE_DOS_HEADER(pImageBase);
    if (pimage_dos_header == NULL) {
            return std::make_pair(names, 1);    // PE ERROR: no DOS header
    }
    else if (pimage_dos_header->e_magic == IMAGE_DOS_SIGNATURE) {
        if (dwFsize < (pimage_dos_header->e_lfanew + sizeof(IMAGE_NT_HEADERS32))) {
            //printf("ONLY DOS error\n");
            return std::make_pair(names, 2);    // PE ERROR: contains only DOS header
        }
    }
    else if (pimage_dos_header->e_magic == IMAGE_NT_SIGNATURE) {
        ;
    }
    else if (pimage_dos_header->e_magic == IMAGE_OS2_SIGNATURE) {
        ;
    }
    else if (pimage_dos_header->e_magic == IMAGE_OS2_SIGNATURE_LE) {
        ;
    }
    else if (pimage_dos_header->e_magic == IMAGE_VXD_SIGNATURE) {
        ;
    }
    else {
        return std::make_pair(names, -1);   // BROKEN PE ERROR: TODO
    }
    pimage_nt_headers = (PIMAGE_NT_HEADERS32)(pImageBase+pimage_dos_header->e_lfanew);
	
    //64bit
    if (pimage_nt_headers->OptionalHeader.Magic == IMAGE_NT_OPTIONAL_HDR64_MAGIC) {
        pimage_nt_headers64 = (PIMAGE_NT_HEADERS64)(pImageBase + pimage_dos_header->e_lfanew);
        if (pimage_nt_headers64->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size == 0) {
            return std::make_pair(names, 3);    // PE ERROR: no IMPORT table (64b)
        }
        dwImportDirectory = RVA2Offset64(pImageBase, pimage_nt_headers64->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
        is64 = true;
    }
    else if (pimage_nt_headers->OptionalHeader.Magic == IMAGE_NT_OPTIONAL_HDR32_MAGIC) {
        //----------------------------------------
        unsigned long size = 0;
        size = pimage_nt_headers->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size;
        if (size == 0) {
            return std::make_pair(names, 4);    // PE ERROR: no IMPORT table (32b)
        }
        dwImportDirectory = RVA2Offset(pImageBase, pimage_nt_headers->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
    }
    else {
        return std::make_pair(names, -2);   // BROKEN PE ERROR: TODO
    }
    
    if (dwImportDirectory == 0)
    {
        return std::make_pair(names, 5);    // PE ERROR: wrong size of IMPORT table
    }
    else if ((dwImportDirectory == -1)){
        return std::make_pair(names, -3);   // BROKEN PE ERROR: TODO
    }
    //----------------------------------------
    pimage_import_descriptor = (PIMAGE_IMPORT_DESCRIPTOR)(pImageBase + dwImportDirectory);
    //----------------------------------------

    while(pimage_import_descriptor->Name!=0)
    {
        pThunk=pImageBase+pimage_import_descriptor->FirstThunk;
        dwThunk = pimage_import_descriptor->FirstThunk;
        pHintName=pImageBase;
        DWORD pomHintName;
        if(pimage_import_descriptor->OriginalFirstThunk!=0)
        {
            if (is64) {

                pomHintName = RVA2Offset64(pImageBase, pimage_import_descriptor->OriginalFirstThunk);
            }
            else {
                 pomHintName = RVA2Offset(pImageBase, pimage_import_descriptor->OriginalFirstThunk);
            }
        }
        else if(pimage_import_descriptor->FirstThunk!=0)
        {
            if (is64) {
                pomHintName = RVA2Offset64(pImageBase, pimage_import_descriptor->FirstThunk);
            }
            else {
                pomHintName = RVA2Offset(pImageBase, pimage_import_descriptor->FirstThunk);
            }
        }
        else{
            return std::make_pair(names, -4);   // BROKEN PE ERROR: TODO
        }
        if((pomHintName == -1)){
            return std::make_pair(names, -5);   // BROKEN PE ERROR: TODO
        }
        pHintName += pomHintName;
        if (is64) {
            pomHintName = RVA2Offset64(pImageBase, pimage_import_descriptor->Name);
            pDllName = pImageBase + pomHintName;
        }
        else {
            pomHintName = RVA2Offset(pImageBase, pimage_import_descriptor->Name);
            pDllName = pImageBase + pomHintName;
        }

        if((pomHintName == -1)){
            return std::make_pair(names, -6);   // BROKEN PE ERROR: TODO
        }
        /*
        callback1(pDllName,
        pimage_import_descriptor->OriginalFirstThunk,
        pimage_import_descriptor->TimeDateStamp,
        pimage_import_descriptor->ForwarderChain,
        pimage_import_descriptor->Name,
        pimage_import_descriptor->FirstThunk);
        */

        if(pDllName == pImageBase){
            return std::make_pair(names, -7);   // BROKEN PE ERROR: TODO
        }
        names.push_back(pDllName);

        pimage_thunk_data=PIMAGE_THUNK_DATA(pHintName);
        pimage_import_descriptor++;
    }
    //----------------------------------------
    return std::make_pair(names, 0);    // PE FORMAT OK!
}
//---------------------------------------------------------
std::pair<std::vector<char*>, int> ITLibrary::GetImportProcName(int iModule)
{
    std::vector<char*> names;

    PCHAR		pThunk;
    DWORD		dwThunk;
    PCHAR		pHintName;
    DWORD		dwAPIaddress;
    ULONGLONG	dwAPIaddress64;
    PCHAR		pDllName;
    PCHAR		pAPIName;
    DWORD		dwImportDirectory;
    int			iCount;
    WORD		Hint;
    bool		is64 = false;

    PCHAR pImageBase = pMem;
    //----------------------------------------
    PIMAGE_IMPORT_DESCRIPTOR	pimage_import_descriptor;
    PIMAGE_THUNK_DATA32			pimage_thunk_data;
    PIMAGE_THUNK_DATA64			pimage_thunk_data64;
    //----------------------------------------
    PIMAGE_DOS_HEADER pimage_dos_header;
    PIMAGE_NT_HEADERS32 pimage_nt_headers;
    PIMAGE_NT_HEADERS64 pimage_nt_headers64;

    pimage_dos_header = PIMAGE_DOS_HEADER(pImageBase);
    if (pimage_dos_header == NULL) {
        return std::make_pair(names, 1);    // PE ERROR: no DOS header
    }
    else if (pimage_dos_header->e_magic == IMAGE_DOS_SIGNATURE) {
        if (dwFsize < (pimage_dos_header->e_lfanew + sizeof(IMAGE_NT_HEADERS32))) {
            //printf("ONLY DOS error\n");
            return std::make_pair(names, 2);    // PE ERROR: contains only DOS header
        }
    }
    else if (pimage_dos_header->e_magic == IMAGE_NT_SIGNATURE) {
        ;
    }
    else if (pimage_dos_header->e_magic == IMAGE_OS2_SIGNATURE) {
        ;
    }
    else if (pimage_dos_header->e_magic == IMAGE_OS2_SIGNATURE_LE) {
        ;
    }
    else if (pimage_dos_header->e_magic == IMAGE_VXD_SIGNATURE) {
        ;
    }
    else {
        return std::make_pair(names, -1);   // BROKEN PE ERROR: TODO
    }
    pimage_nt_headers = (PIMAGE_NT_HEADERS32)(pImageBase + pimage_dos_header->e_lfanew);
    //64bit
    if (pimage_nt_headers->OptionalHeader.Magic == IMAGE_NT_OPTIONAL_HDR64_MAGIC) {
        pimage_nt_headers64 = (PIMAGE_NT_HEADERS64)(pImageBase + pimage_dos_header->e_lfanew);
        if (pimage_nt_headers64->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size == 0) {
            return std::make_pair(names, 3);    // PE ERROR: no IMPORT table (64b)
        }
        dwImportDirectory = RVA2Offset64(pImageBase, pimage_nt_headers64->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
        is64 = true;
    }
    else if (pimage_nt_headers->OptionalHeader.Magic == IMAGE_NT_OPTIONAL_HDR32_MAGIC) {
        //----------------------------------------
        if (pimage_nt_headers->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].Size == 0) {
            return std::make_pair(names, 4);    // PE ERROR: no IMPORT table (32b)
        }
        dwImportDirectory = RVA2Offset(pImageBase, pimage_nt_headers->OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT].VirtualAddress);
    }
    else {
        return std::make_pair(names, -2);   // BROKEN PE ERROR: TODO
    }
    
    if (dwImportDirectory == 0)
    {
        return std::make_pair(names, 5);    // PE ERROR: wrong size of IMPORT table
    }
    else if ((dwImportDirectory == -1)){
        return std::make_pair(names, -3);   // BROKEN PE ERROR: TODO
    }
    //----------------------------------------
    pimage_import_descriptor=(PIMAGE_IMPORT_DESCRIPTOR)(pImageBase+dwImportDirectory);
    //----------------------------------------
    iCount = 0;

    while(pimage_import_descriptor->Name!=0)
    {
        pThunk=pImageBase+pimage_import_descriptor->FirstThunk;
        if(pimage_import_descriptor->OriginalFirstThunk!=0)
        {
            dwThunk = pimage_import_descriptor->OriginalFirstThunk;
        }
        else
        {
            dwThunk = pimage_import_descriptor->FirstThunk;
        }
        pHintName=pImageBase;
        DWORD pomHintName;
        if (pimage_import_descriptor->OriginalFirstThunk != 0)
        {
            if (is64) {
                pomHintName = RVA2Offset64(pImageBase, pimage_import_descriptor->OriginalFirstThunk);
            }
            else {
                pomHintName = RVA2Offset(pImageBase, pimage_import_descriptor->OriginalFirstThunk);
            }
        }
        else if(pimage_import_descriptor->FirstThunk != 0)
        {
            if (is64) {
                pomHintName = RVA2Offset64(pImageBase, pimage_import_descriptor->FirstThunk);
            }
            else {
                pomHintName = RVA2Offset(pImageBase, pimage_import_descriptor->FirstThunk);
            }
        }
        else{
            return std::make_pair(names, -4);   // BROKEN PE ERROR: TODO
        }
        pHintName += pomHintName;
        if((pomHintName == -1)){
            return std::make_pair(names, -5);   // BROKEN PE ERROR: TODO
        }

        if (is64) {
            pomHintName = RVA2Offset64(pImageBase, pimage_import_descriptor->Name);
            pDllName = pImageBase + pomHintName;
        }
        else {
            pomHintName = RVA2Offset(pImageBase, pimage_import_descriptor->Name);
            pDllName = pImageBase + pomHintName;
        }
        if((pomHintName == -1)){
            return std::make_pair(names, -6);   // BROKEN PE ERROR: TODO
        }

        if(pHintName == pImageBase){
            return std::make_pair(names, -7);   // BROKEN PE ERROR: TODO
        }
        pimage_thunk_data=PIMAGE_THUNK_DATA32(pHintName);
        if (is64) {
            pimage_thunk_data64 = PIMAGE_THUNK_DATA64(pHintName);
            if (iModule == iCount)
            {
                while (pimage_thunk_data64->u1.AddressOfData != 0)
                {
                    dwAPIaddress64 = pimage_thunk_data64->u1.AddressOfData;
                    /*
                    if(dwAPIaddress64 > dwFsize){
                        return std::make_pair(names, -8);   // BROKEN PE ERROR: TODO
                    }
                    */
                    if ((dwAPIaddress64&IMAGE_ORDINAL_FLAG64) == IMAGE_ORDINAL_FLAG64) {
                        IMAGE_SNAP_BY_ORDINAL64(dwAPIaddress64);
                    }
                    else
                    {
                        pomHintName = RVA2Offset64(pImageBase, dwAPIaddress64);
                        if((pomHintName == -1)){
                            return std::make_pair(names, -9);   // BROKEN PE ERROR: TODO
                        }
                        pAPIName = pImageBase + pomHintName + 2;
                        memcpy(&Hint, pAPIName - 2, 2);
                        //callback2(dwThunk, RVA2Offset(pImageBase,dwThunk),dwAPIaddress, Hint ,pAPIName, 0);
                        names.push_back(pAPIName);
                    }
                    pThunk += 8;
                    dwThunk += 8;
                    pHintName += 8;
                    pimage_thunk_data64++;
                }
            }
            pimage_import_descriptor++;
            iCount++;

        }
        else if(iModule==iCount)
        {
            while(pimage_thunk_data->u1.AddressOfData!=0)
            {
                dwAPIaddress=pimage_thunk_data->u1.AddressOfData;
                /*
                if(dwAPIaddress > dwFsize){
                    return std::make_pair(names, -10);   // BROKEN PE ERROR: TODO
                }
                */
                if((dwAPIaddress&IMAGE_ORDINAL_FLAG32)==IMAGE_ORDINAL_FLAG32)
                {
                    IMAGE_SNAP_BY_ORDINAL32(dwAPIaddress);
                    //callback2(dwThunk, RVA2Offset(pImageBase,dwThunk),0x80000000&dwAPIaddress, 0 ,NULL, dwAPIaddress);
                }
                else
                {
                    /*
                    if(!isNameOkay(pImageBase, RVA2Offset(pImageBase, dwAPIaddress))){
                        return std::make_pair(names, -11);   // BROKEN PE ERROR: TODO
                    }
                    */

                    pomHintName = RVA2Offset(pImageBase, dwAPIaddress);
                    if((pomHintName == -1)){
                        return std::make_pair(names, -12);   // BROKEN PE ERROR: TODO
                    }
                    pAPIName = pImageBase + pomHintName + 2;
                    memcpy(&Hint, pAPIName-2, 2);
                    //callback2(dwThunk, RVA2Offset(pImageBase,dwThunk),dwAPIaddress, Hint ,pAPIName, 0);
                    names.push_back(pAPIName);
                }
                pThunk+=4;
                dwThunk+=4;
                pHintName+=4;
                pimage_thunk_data++;
            }
        }
        pimage_import_descriptor++;
        iCount++;
    }
    //----------------------------------------
    return std::make_pair(names, 0);    // PE FORMAT OK!
}
