QT += widgets

SOURCES = main.cpp \
    PEHeader.cpp \
    itlib.cpp \
    peliberr.cpp \
    stdafx.cpp

# install
target.path = $$[QT_INSTALL_EXAMPLES]/widgets/tutorials/widgets/childwidget
INSTALLS += target

HEADERS += \
    PEHeader.h \
    itlib.h \
    peliberr.h \
    resource.h \
    stdafx.h
	
CONFIG += static
